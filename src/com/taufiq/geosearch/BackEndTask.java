package com.taufiq.geosearch;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class BackEndTask extends AsyncTask<String, Void, List<DataModel>> {
	
	private List<DataModel> listData ;
	private Context context;
	private IOnTaskComplete listener;
	private ProgressDialog progressDialog;
	
	public BackEndTask(Context context, IOnTaskComplete listener)
	{
		this.context = context;
		this.listener = listener;
		
		
	}
	
	
	@Override
	protected List<DataModel> doInBackground(String... params) {
		// TODO Auto-generated method stub
		this.listData = new ArrayList<DataModel>();
		this.listData.clear();
		
		JSONParser jParser = new JSONParser();
		
		JSONObject jsonRoot = jParser.getJSONFromUrl(params[0]);
		
		try 
		{
			
			
			listData.add(convertResponse(jsonRoot));
			
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
		
		
		return this.listData;
	}
	
	
	@Override
	protected void onPreExecute() {
		
		progressDialog = new ProgressDialog(this.context);
		progressDialog.setMessage("Please wait");
		progressDialog.show();
		
		
		super.onPreExecute();
		
	}
	
	@Override
	protected void onPostExecute(List<DataModel> result) {
		
		if(progressDialog.isShowing())
			progressDialog.dismiss();
		
		listener.onComplete();
	}
	
	
	
	// Converts the JSONObject into DataModel object 
	
	private DataModel convertResponse(JSONObject obj) throws JSONException {
		
		String ip = obj.getString("ip");
		String countryCode = obj.getString("country_code");
		String countryName = obj.getString("country_name");
		String regionCode = obj.getString("region_code");
		String regionName = obj.getString("region_name");
		String city = obj.getString("city");
		String zipCode = obj.getString("zipcode");
		String latitude = String.valueOf(obj.getDouble("latitude"));
		String longitude = String.valueOf(obj.getDouble("longitude"));
		String metroCode = obj.getString("metro_code");
		String areaCode = obj.getString("area_code");
		
		if(ip.length() <= 0 || ip == null)
			ip = AppMessages.NOT_FOUND_API_MESSAGE;
		if(countryCode.length() <= 0 || countryCode == null)
			countryCode = AppMessages.NOT_FOUND_API_MESSAGE;
		if(countryName.length() <= 0 || countryName == null)
			countryName = AppMessages.NOT_FOUND_API_MESSAGE;
		if(regionCode.length() <= 0 || regionCode == null)
			regionCode = AppMessages.NOT_FOUND_API_MESSAGE;
		if(regionName.length() <= 0 || regionName == null)
			regionName = AppMessages.NOT_FOUND_API_MESSAGE;
		if(city.length() <= 0 || city == null)
			city = AppMessages.NOT_FOUND_API_MESSAGE;
		if(zipCode.length() <= 0 || zipCode == null)
			zipCode = AppMessages.NOT_FOUND_API_MESSAGE;
		if(latitude.length() <= 0 || latitude == null)
			latitude = AppMessages.NOT_FOUND_API_MESSAGE;
		if(longitude.length() <= 0 || longitude == null)
			longitude = AppMessages.NOT_FOUND_API_MESSAGE;
		if(metroCode.length() <= 0 || metroCode == null)
			metroCode = AppMessages.NOT_FOUND_API_MESSAGE;
		if(areaCode.length() <= 0 || areaCode == null)
			areaCode = AppMessages.NOT_FOUND_API_MESSAGE;

        return new DataModel(ip, countryCode, countryName, regionCode, regionName, city, zipCode, latitude, longitude, metroCode, areaCode);
    }
	
	
	
	/**
	 * @param 
	 * @return List<DataModel> listData
	 * 
	 * */
	
	public List<DataModel> getList()
	{
		return this.listData;
	}
	

}
