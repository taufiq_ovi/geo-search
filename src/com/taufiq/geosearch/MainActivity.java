package com.taufiq.geosearch;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MainActivity extends Activity implements IOnTaskComplete{
	
	
	private BackEndTask backEndTask;
	
	
	private AutoCompleteTextView txtInput;
	private Button btnSubmit;
	
	private TextView txtIP;
	private TextView txtCountryCode;
	private TextView txtCountryName;
	private TextView txtRegionCode;
	private TextView txtRegionName;
	private TextView txtCity;
	private TextView txtZipCode;
	private TextView txtLatitude;
	private TextView txtLongitude;
	private TextView txtMetroCode;
	private TextView txtAreaCode;
	private RelativeLayout layoutInfoHolder;
	
	
	private static final String PATTERN = 
	        "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
	        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
	        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
	        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
	
	
	
	private List<String> listUsedAddress = new ArrayList<String>();
	
	private ArrayAdapter<String> adapter;
	
	private Animation shakeAnimation ;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.layout_mainactivity);
		
		
		initObject();
		initAction();
		
		
		
	}
	
	//Initiating all the objects from layout and others
	private void initObject()
	{
		this.txtInput = (AutoCompleteTextView)this.findViewById(R.id.txtInput);
		this.btnSubmit = (Button)this.findViewById(R.id.btnSubmit);
		
		this.txtIP = (TextView)this.findViewById(R.id.txtIP);
		this.txtCountryCode = (TextView)this.findViewById(R.id.txtCountryCode);
		this.txtCountryName = (TextView)this.findViewById(R.id.txtCountryName);
		this.txtRegionCode = (TextView)this.findViewById(R.id.txtRegionCode);
		this.txtRegionName = (TextView)this.findViewById(R.id.txtRegionName);
		this.txtCity = (TextView)this.findViewById(R.id.txtCity);
		this.txtZipCode = (TextView)this.findViewById(R.id.txtZipCode);
		this.txtLatitude = (TextView)this.findViewById(R.id.txtLatitude);
		this.txtLongitude = (TextView)this.findViewById(R.id.txtLongitude);
		this.txtMetroCode = (TextView)this.findViewById(R.id.txtMetroCode);
		this.txtAreaCode = (TextView)this.findViewById(R.id.txtAreaCode);
		this.layoutInfoHolder = (RelativeLayout)this.findViewById(R.id.layoutInfoHolder);
		
		
		listUsedAddress.clear();
		if(getPrefData("USED_SITES").length() > 0)
		{
			Type t = new TypeToken<List<String>>() {}.getType();
			List<String> list =  (List<String>)new Gson().fromJson(getPrefData("USED_SITES"), t);
			listUsedAddress.addAll(list);
		}
		
		
		this.adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_dropdown_item_1line, listUsedAddress);
		txtInput.setAdapter(this.adapter);
		
		shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);
       
	}
	
	//Initiating all actions 
	private void initAction()
	{
		
		this.btnSubmit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Log.v("ADDRESS", "is: "+txtInput.getText().toString().trim());

				
				//Online i.e. web api is called
				if(isNetworkConnected())
				{
					String siteAddress = txtInput.getText().toString();
					
					//Checking for a valid url if not, shows a toast message
					
					if(Patterns.WEB_URL.matcher(siteAddress).matches() || validateIP(siteAddress))
					{
						if(siteAddress.contains("https://")) //Because freegeoip.net doesn't accept https://
						{
							siteAddress = siteAddress.substring(7, siteAddress.length());
						}
						
						if(siteAddress.length() > 0 )
						{
							
							backEndTask = new BackEndTask(MainActivity.this, MainActivity.this);
							backEndTask.execute("https://freegeoip.net/json/"+siteAddress);
						}
						
						else
						{
							txtInput.startAnimation(shakeAnimation);
							
							Toast.makeText(MainActivity.this, AppMessages.WRONG_IP_URL, Toast.LENGTH_SHORT).show();
						}
						
					}
					
					
					else
					{
						txtInput.startAnimation(shakeAnimation);
						
						Toast.makeText(MainActivity.this, AppMessages.WRONG_IP_URL, Toast.LENGTH_SHORT).show();
					}
					
				}
				
				//Offline i.e. show data from preference cache if available
				else
				{
					if(getPrefData(txtInput.getText().toString().trim()).length() > 0)
					{
						Toast.makeText(MainActivity.this, AppMessages.RETRIEVE_OFFLINE, Toast.LENGTH_LONG).show();
						
						Type t = new TypeToken<List<DataModel>>() {}.getType();
						List<DataModel> list =  (List<DataModel>)new Gson().fromJson(getPrefData(txtInput.getText().toString().trim()), t);
						populateData(list);
					}
					else
					{
						Toast.makeText(MainActivity.this, AppMessages.RETRIEVE_OFFLINE_FAILED, Toast.LENGTH_LONG).show();
					}
					
					
				}
				
				
				if(!listUsedAddress.contains(txtInput.getText().toString()))
				{
					listUsedAddress.add(txtInput.getText().toString());
					savePrefData("USED_SITES", new Gson().toJson(listUsedAddress));
					
				}
				
				adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_dropdown_item_1line, listUsedAddress);
				txtInput.setAdapter(adapter);
				
				
			}
		});
		
		
		
	
		
		
	}
	
	
	private boolean validateIP(String ip)
	{          
	      Pattern pattern = Pattern.compile(PATTERN);
	      Matcher matcher = pattern.matcher(ip);
	      return matcher.matches();             
	}
	

	@Override
	public void onComplete() {
		// TODO Auto-generated method stub
		String value = new Gson().toJson(this.backEndTask.getList());
		savePrefData(this.txtInput.getText().toString().trim(), value);
		
		populateData(this.backEndTask.getList());
		
		
		
		
	}
	
	
	//Populating data after fetching and showing in views
	private void populateData(List<DataModel> list)
	{
		
		this.layoutInfoHolder.setVisibility(View.VISIBLE);
		
		this.txtIP.setText(list.get(0).getIp());
		this.txtCountryCode.setText(list.get(0).getCountryCode());
		this.txtCountryName.setText(list.get(0).getCountryName());
		this.txtRegionCode.setText(list.get(0).getRegionCode());
		this.txtRegionName.setText(list.get(0).getRegionName());
		this.txtCity.setText(list.get(0).getCity());
		this.txtZipCode.setText(list.get(0).getZipCode());
		this.txtLatitude.setText(list.get(0).getLatitude());
		this.txtLongitude.setText(list.get(0).getLongitude());
		this.txtMetroCode.setText(list.get(0).getMetroCode());
		this.txtAreaCode.setText(list.get(0).getAreaCode());
		
		String value = new Gson().toJson(list);
		savePrefData(list.get(0).getIp(), value);
		
		
		
	}
	
	//Checking for potential internet connectivity 
	private boolean isNetworkConnected() 
	{
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni == null) 
		{
			return false;
		} 
		else
			return true;
	}
	
	//Saving data to preference 
	private void savePrefData(String key, String value)
	{
		
		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	//Getting data from preference
	private String getPrefData(String key)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this); 
		String value = prefs.getString(key, "");
		
		return value;
		
	}
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if (keyCode == KeyEvent.KEYCODE_BACK) 
		{
			showExitDialog();
		}
		return super.onKeyDown(keyCode, event);
	}

	
	//Showing a popup dialog and prompt user  
	private void showExitDialog() {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				MainActivity.this);

		alertDialogBuilder.setTitle("Spell Champs");

		alertDialogBuilder
				.setMessage(AppMessages.HOME_PAGEACTIVITY_EXIT_MESSAGE)
				.setCancelable(false)
				.setIcon(R.drawable.ic_launcher)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

					
					
					@Override
					public void onClick(final DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						
						MainActivity.this.finish();

					}
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(final DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();

					}
				});

		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.show();

	}
	
	
	@Override
	protected void onDestroy()
	{
		AppConstants.unbindDrawables(this.findViewById(R.id.rootLayout));
		System.gc();
		
		super.onDestroy();
	}

}
