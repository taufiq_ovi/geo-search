package com.taufiq.geosearch;

public class DataModel {
	
	
	private String ip = "";
	private String countryCode = "";
	private String countryName = "";
	private String regionCode = "";
	private String regionName = "";
	private String city = "";
	private String zipCode = "";
	private String latitude = "";
	private String longitude = "";
	private String metroCode = "";
	private String areaCode = "";
	
	
	public DataModel(String ip, String countryCode, String countryName, String regionCode, String regionName, String city, String zipCode,
			String latitude, String longitude, String metroCode, String areaCode)
	{
		this.ip = ip;
		this.countryCode = countryCode;
		this.countryName = countryName;
		this.regionCode = regionCode;
		this.regionName = regionName;
		this.city = city;
		this.zipCode = zipCode;
		this.latitude = latitude;
		this.longitude = longitude;
		this.metroCode = metroCode;
		this.areaCode = areaCode;
	}
	
	
	/**
	 * @param
	 * @return String ip
	 * 
	 * 
	 * */
	
	public String getIp()
	{
		return ip;
	}
	
	
	/**
	 * @param
	 * @return String countryCode
	 * 
	 * 
	 * */
	public String getCountryCode() 
	{
		return countryCode;
	}

	
	/**
	 * @param
	 * @return String countryName
	 * 
	 * 
	 * */
	public String getCountryName() 
	{
		return countryName;
	}

	
	/**
	 * @param
	 * @return String regionCode
	 * 
	 * 
	 * */
	public String getRegionCode() 
	{
		return regionCode;
	}

	
	/**
	 * @param
	 * @return String regionName
	 * 
	 * 
	 * */
	public String getRegionName()
	{
		return regionName;
	}

	
	/**
	 * @param
	 * @return String city
	 * 
	 * 
	 * */
	public String getCity() 
	{
		return city;
	}

	
	/**
	 * @param
	 * @return String zipCode
	 * 
	 * 
	 * */
	public String getZipCode() 
	{
		return zipCode;
	}

	
	/**
	 * @param
	 * @return String latitude
	 * 
	 * 
	 * */
	public String getLatitude() 
	{
		return latitude;
	}


	/**
	 * @param
	 * @return String longitude
	 * 
	 * 
	 * */
	public String getLongitude() 
	{
		return longitude;
	}

	
	/**
	 * @param
	 * @return String metroCode
	 * 
	 * 
	 * */
	public String getMetroCode() 
	{
		return metroCode;
	}


	/**
	 * @param
	 * @return String areaCode
	 * 
	 * 
	 * */
	public String getAreaCode() 
	{
		return areaCode;
	}


	

}
