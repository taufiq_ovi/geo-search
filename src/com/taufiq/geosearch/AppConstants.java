package com.taufiq.geosearch;

import android.view.View;
import android.view.ViewGroup;

public class AppConstants {
	
	
	public static void unbindDrawables(View view) 
	{
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

}
