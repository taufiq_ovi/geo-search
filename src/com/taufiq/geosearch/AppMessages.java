package com.taufiq.geosearch;

public class AppMessages {
	
	public static final String WRONG_IP_URL = "You have entered wrong IP address or URL link; please provide a valid address";
	public static final String RETRIEVE_OFFLINE = "Retrieving data from offline cache";
	public static final String RETRIEVE_OFFLINE_FAILED = "Failed to retrieving data from offline, this site may never be called (cached) before";
	public static final String HOME_PAGEACTIVITY_EXIT_MESSAGE = "Would you like to exit 'Geo Search'";
	public static final String NOT_FOUND_API_MESSAGE = "Not found from api!";

}
